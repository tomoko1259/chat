import socket
import threading

HOST = '127.0.0.1' #Online use ip address ifconfig
PORT = 9090
FORMAT = 'utf-8'

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((HOST, PORT))

server.listen()

clients = []
nicknames = []

#brodcast (send message to all connected clients)
def broadcast(message):
    for client in clients:
        client.send(message)

#handle (handle each connection to the clients)
def handle(client):
    while True:
        try:
            message = client.recv(1024)
            print(f"{nicknames[clients.index(client)]} says {message}")
            broadcast(message)
        except:
            #find client and nickname in lists and remove them
            index = clients.index(client)
            clients.remove(client)
            client.close()
            nickname = nicknames[index]
            broadcast(f"{nickname} has left the chat")
            nicknames.remove(nickname)
            break

#receive (accept new connections)
def receive(): #main function running in main thread
    while True:
        client, address = server.accept()
        print(f"Connected with {str(address)}!")

        client.send("NAME".encode(FORMAT))
        nickname = client.recv(1024)

        nicknames.append(nickname)
        clients.append(client)

        print(f"Nickname of the client is {nickname}")
        broadcast(f"{nickname} connected to the server!\n".encode(FORMAT))
        client.send("Connected to the server!".encode(FORMAT))

        thread = threading.Thread(target=handle, args=(client,))
        thread.start()

print("Server running...")
receive()
